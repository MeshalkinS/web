   
function main(){

		var year = document.getElementById("year").value;
		var month = document.getElementById("month").value;
		var day = document.getElementById("day").value;
		
		var today_date = new Date();
		var date = new Date(year, month, day);
							
		var difference = date.getTime() - today_date.getTime();
   
		if(checkDate(year, month, day) && (difference > 0)){
			start_count(difference, year, month, day);
		}
		else{
				alert('Введена некорректная дата');
		}
		
	}
	
function start_count(difference, year, month, day) {
		
    var block = document.getElementById('sample_countdown');
	var block_text = document.getElementById('block_text');

    simple_timer((difference/1000), block);
}

  function simple_timer(sec, block) {
  
  
    var time  = sec;
   
	var year    = parseInt(time / (3600*24*365));
    if ( day < 1 ) day = 0;
    time = parseInt(time - year * (3600*24*365));
    if ( day < 10 ) year = '0'+year;
	
	var day    = parseInt(time / (3600*24));
    if ( day < 1 ) day = 0;
    time = parseInt(time - day * (3600*24));
    if ( day < 10 ) day = '0'+day;
	
    var hour    = parseInt(time / 3600);
    if ( hour < 1 ) hour = 0;
    time = parseInt(time - hour * 3600);
    if ( hour < 10 ) hour = '0'+hour;
 
    var minutes = parseInt(time / 60);
    if ( minutes < 1 ) minutes = 0;
    time = parseInt(time - minutes * 60);
    if ( minutes < 10 ) minutes = '0'+minutes;
 
    var seconds = time;
    if ( seconds < 10 ) seconds = '0'+seconds;
		
		block_text.innerHTML = year+' лет  '+day+' дней';
        block.innerHTML =hour+':'+minutes+':'+seconds;
        sec--;
 
        if ( sec > 0 ) {
            setTimeout(function(){
			simple_timer(sec, block);
			}, 1000);
        } else {
            alert('Конец');
        }
}

function checkDate (year, month, day) {

		var flag = true; 

		 if (month < 1 || month > 12){
             flag = false;
         }
		 
		 if (day < 1 || day > 31){
		     flag = false;
		 }
      
         if ((month == 4 || month == 6 || month == 9 || month == 11) &&
             (day == 31)) {
             flag = false;
         }
		 
         if (month == 2) {
             var leap = (year % 4 == 0 &&
                        (year % 100 != 0 || year % 400 == 0));
             if (day>29 || (day == 29 && !leap)) {
                 flag = false;
             }
         }
         return flag;
    }